import json
from django.db import transaction
from protfamily.models import Family, Alignment, Stat, Tree

DATA_REPO = '../django-vue.js/data/'

ALIGNEMENTS_REPO = DATA_REPO+'/alignments/'
STATS_REPO = DATA_REPO+'/stats/'
TREES_REPO = DATA_REPO+'/trees/'

FAMILIES = ['orthomcl10','orthomcl12','orthomcl17', 'orthomcl909']

with transaction.atomic() :
    for fam in FAMILIES :
        family_object = Family.objects.create(name=fam)
        
        align_fh = open('{0}{1}.fasta'.format(ALIGNEMENTS_REPO, fam),'r')
        align_object = None
        sequence = ""
        for line in align_fh.readlines() :
            if line.startswith('>') :
                if align_object != None:
                    align_object.seq = sequence
                    align_object.save()
                    sequence = ""
                align_object = Alignment()
                align_object.seq_id = line[1:].strip()
                align_object.family = family_object
            else :
                sequence = sequence+line.strip()
        align_object.seq = sequence
        align_object.save()
        align_fh.close()
        
        stat_fh = open('{0}{1}.json'.format(STATS_REPO, fam),'r')
        stats_data = json.load(stat_fh)
        Stat.objects.create(family = family_object,
                            proteins = stats_data['proteins'],
                            species = stats_data['species'])
        stat_fh.close()
        
        tree_fh = open('{0}{1}.newick'.format(TREES_REPO, fam),'r')
        Tree.objects.create(family = family_object,
                            tree = tree_fh.read().strip())
        tree_fh.close()
        
        
        